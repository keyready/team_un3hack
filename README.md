# ____un3Hack__


## Gitlab-страничка команды __un3Hack
Здесь Вы можете найти основную информацию об участниках, полученных достижениях и наработок команды

## Branches
* [ ] [Hackathons](https://gitlab.com/keyready/team_un3hack/-/tree/Hackathons) - *наши достижения на хакатонах*
* [ ] [Portfolio](https://gitlab.com/keyready/team_un3hack/-/tree/Participants_Portfolio) - *портфолио участников*
* [ ] [Designs](https://gitlab.com/keyready/team_un3hack/-/tree/Design) - *дизайны проектов в формате .fig*


## Участники команды

| Участник                                               | Направление          | Технологии                                         |
|:-------------------------------------------------------|----------------------|:---------------------------------------------------|
| [Поляков Дмитрий](https://gitlab.com/_nightboyfriend_) | Презентации          | PowerPoint, Adobe Illustrator                      |
| [Горбанева Наталия](https://gitlab.com/natalingor)     | Дизайнер             | Figma, Tilda, Adobe Photoshop                      |
| [Тыренко Даниил](https://gitlab.com/anyquestions69)    | Backend-разработчик  | NodeJs (Express), PHP, mySQL, postgreSQL           |
| [Кофанов Валентин](https://gitlab.com/VALIKO136)       | Backend-разработчик  | Python Django, NodeJS (Express), mySQL, postgreSQL |
| [Корчак Родион](https://gitlab.com/keyready)           | Frontend-разработчик | ReactJs, Typescript, NodeJS (Express)              |